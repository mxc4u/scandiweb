Updated on 31.12.2021!

# Hey Scandiweb!
 
Link to the react website: https://simmerscandiweb.vercel.app/

Frontend is hosted on vercel.app.
Backend is hosted on 000webhost.

PHP code is stored in "PHP" folder. All the PHP code that is not in folder "Classes" is used for connection between frontend and backend. The PHP code that "Classes" holds is for connecting to DB, abstract product class etc.